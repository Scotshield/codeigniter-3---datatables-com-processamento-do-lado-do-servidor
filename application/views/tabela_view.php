<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en" ng-app="sigep">
	<head>
		<title>Tabela</title>
		<!-- PARA O DATATABLES FUNCIONAR É NECESSÁRIO IMPORTAR O JQUERY, E OS ARQUIVOS DE ESTIVO E JS DO DATATABLES -->
		<link rel="stylesheet" type="text/css" href="<?= base_url("assets/DataTables/datatables.min.css");?>">
		<script src="<?= base_url("assets/js/jquery-3.3.1.min.js"); ?>"></script>
		<script type="text/javascript" charset="utf8" src="<?= base_url("assets/DataTables/datatables.min.js");?>"></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4 class="page-head-line"><?= $title ?></h4>
				</div>
			</div>

			<table class="table table-bordered table-sm table-striped table-hover psDataTable" id="tabela">
				<thead>
					<tr>
						<th>Matricula</th>
						<th>Nome</th>
						<th>CPF</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
		<script>
		$(document).ready(function() {
			var dataTable = $('#tabela').DataTable( {
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: "<?= base_url(); ?>index.php/api/pessoas",
					type: "POST"
				}
			});
			/*
  		 Se tivéssemos 5 colunas e não quiséssemos ordenar elas pela primeira e pela terceira posição, devemos inserir a seguinte configuração:

			 "columnDefs":[
        	{
            "targets":[0, 2],
            "orderable":false,
         	},
       	],

			 Essa observação é um complemento ao comentário feito no model

			 Com essa configuração, a declaração do dataTables completa seria algo como:

				var dataTable = $('#tabela').DataTable( {
		      "processing": true,
		      "serverSide": true,
		      "order": [],
		      "ajax": {
		        url: "<?= base_url(); ?>index.php/os/dataTablesOs",
		        type: "POST"
		  		},"columnDefs":[
		            {
		                 "targets":[0, 2],
		                 "orderable":false,
		            },
		       ],
				 });
   		 *
			 */
		});
		</script>
	</body>
</html>
