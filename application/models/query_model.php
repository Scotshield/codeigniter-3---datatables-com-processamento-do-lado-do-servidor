<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Query_model extends CI_Model{
	var $table = "tabela_qualquer";
	var $select_column = array("nom", "matricula", "num_cpf");
	var $order_column = array("nom", "matricula", "num_cpf"); //deve estar na mesma ordem que aparece na view. Essa variável serve para quando clicar em ordenar uma coluna, toda tabela seja ordenada por seu atributo.

	/*
	 * Se tivéssemos um caso com 5 colunas (índices 0 a 4), onde não fosse desejável ordenar primeira e pela terceira coluna devemos por null na posição dessa coluna. Exemplo:
	 * var $order_column = array(null, "nom", null, "matricula", "num_cpf");
	 * Essa declaração deve ser complementada na view com "columnDefs"
	 */

	//FUNÇÃO QUE CRIA A QUERY
	function make_query(){
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		if(isset($_POST['search']['value'])){
			$this->db->like('upper(nom)', strtoupper($_POST['search']['value']));
			$this->db->or_like('num_cpf', $_POST['search']['value']);
		}
		if(isset($_POST['order'])){
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else{
			$this->db->order_by('nom', 'asc');
		}
	}

	//FUNÇÃO QUE RETORNA A QUANTIDADE CERTA DE PAGINAÇÃO DA TABELA
	function make_datatables(){
		$this->make_query();
		if($_POST["length"] != -1){
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	//OBTÉM INTEIRO COM O NÚMERO DE ROWS APÓS FILTRAR
	function get_filtered_data(){
		$this->make_query();
		$query = $this->db->get();
		return $query->num_rows();
   }

	//OBTÉM INTEIRO COM O TOTAL DE LINHAS NA TABELA
	function get_all_data(){
		$this->db->select("count(*) num_rows");
		$this->db->from($this->table);
		$query = $this->db->get()->row();
		return $query->num_rows;
	}
}
