<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->model('query_model'); //importa o model 
	}

	function index(){
		$data['title'] = "Funcionários"; 
		$this->load->view('tabela_view', $data);
	}

	function pessoas(){
		$servidores = $this->query_model->make_datatables(); //obtem os dados (todos)
		$data = array(); //declara array vazio que vai receber os dados 
		foreach($servidores as $servidor){ //percorre item a item do resultado 
			
			$sub_array = array(); //cria array que equivale a uma linha da tabela
			$sub_array[] = $servidor->MATRICULA; //monta os atributos da tabela. Aqui pode ser inserido HTML também
			$sub_array[] = $servidor->NOM;
			$sub_array[] = $servidor->NUM_CPF;
			$data[] = $sub_array; //popula o array que equivale à tabela com o array que equivale à linha
		}
		
		//prepara a saída.
		$output = array(
			'draw'				=>	intval($_POST["draw"]),
			'recordsTotal'		=> 	$this->query_model->get_all_data(),
			'recordsFiltered'	=> 	$this->query_model->get_filtered_data(),
			'data'				=> 	$data
		);
		
		//retorna o resultado em JSON
		return $this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($output));
	}
}
