# CodeIgniter 3 - DataTables do lado do Servidor (DataTables server-side)

Este é um exemplo de como implementar o DataTables com processamento do lado do servidor usando o framework CodeIgniter 3.

O model possui as funções de pesquisa no Banco de Dados, de acordo com a necessidade do DataTables, que envia algumas variáveis ao realizar o request.

O controller monta a visualização.

A view apresenta o resultado ao usuário.

https://datatables.net/
Versão do DataTables: 1.10.19

Esse código eu vi inicialmente em [site](https://www.webslesson.info/2016/12/datatables-server-side-processing-in-codeigniter-using-ajax.html), mas algumas coisas não funcionaram, então o adaptei.
